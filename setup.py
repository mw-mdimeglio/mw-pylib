from setuptools import setup

setup(name='mwpyutils',
    version='0.2',
    description='Testing installation of Package',
    url='#',
    author='john doe',
    author_email='john.doe@email.com',
    license='MIT',
    packages=['mw_fileIO', 'mw_string'],
    zip_safe=False)